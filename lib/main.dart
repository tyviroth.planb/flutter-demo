import 'package:flutter/material.dart';
import 'package:flutterdemo/configure/localization/language_wrapper.dart';
import 'package:flutterdemo/configure/localization/local_share_preference.dart';
import 'package:flutterdemo/configure/routes/route_utils.dart';
import 'package:flutterdemo/provider/app_language.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  String locale = await getLanguage();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider.value(value: AppLanguage()),
    ],
    child: LanguageWrapper(
      locale: locale,
      child: MyApp(),
    ),
  ));
}

Future<String> getLanguage() async {
  String language = await LocalSharePreference.readLanguageFromSharePreference();
  if (language == null) {
    language = 'en';
  }
  return language;
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      routes: RouteUtil.getRoute(),
    );
  }
}
