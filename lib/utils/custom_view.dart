import 'package:flutter/cupertino.dart';
import 'package:flutterdemo/utils/image_path.dart';
import 'package:flutterdemo/utils/text_path.dart';

class CustomView {

  static Widget emptyDataView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset("${ImagePath.img_no_data}", width: 128, height: 128,),
        Padding(
          padding: EdgeInsets.only(top: 24),
          child: Text("${TextPath.no_data}"),
        )
      ],
    );
  }

}