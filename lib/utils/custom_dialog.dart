import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDialog {

  static Future<void> confirmDialog(
      {BuildContext context,
        String title = 'Are you sure to send request of updating profile ?',
        String acceptButtonTitle = 'OK',
        String denyButtonTitle = 'Cancel',
        Function acceptOnClick,
        Function denyOnclick}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12))
          ),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  child: Text('$title',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16)),
                  padding: EdgeInsets.only(top: 10, bottom: 30),
                ),
                Padding(
                  padding: EdgeInsets.all(0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Material(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Colors.grey.shade400,
                          child: InkWell(
                            onTap: denyOnclick,
                            child: Center(
                              child: Text(denyButtonTitle),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 100,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Material(
                          elevation: 0,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Theme.of(context).primaryColor,
                          child: InkWell(
                            onTap: acceptOnClick,
                            child: Center(
                              child: Text(acceptButtonTitle, style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }


}
