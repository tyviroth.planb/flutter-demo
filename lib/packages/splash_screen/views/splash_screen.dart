import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutterdemo/packages/bottom_navigation/views/bottom_navigation_screen.dart';
import 'package:flutterdemo/packages/login/third_party/views/social_login_screen.dart';
import 'package:flutterdemo/packages/menu_drawer/views/drawer_screen.dart';
import 'package:flutterdemo/packages/splash_screen/util/motion.dart';
import 'package:flutterdemo/packages/splash_screen/views/home_screen.dart';
import 'package:flutterdemo/packages/web_view/views/WebViewScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with WidgetsBindingObserver{
  MotionExitConfigurations motionExitConfigurations;
  int animDuration = 2000;
  var notesReference = FirebaseDatabase.instance.reference().child("start");
  StreamSubscription<Event> onNoteAddedSubscription;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
    motionExitConfigurations = MotionExitConfigurations(durationMs: 500, mode: MotionMode.FADE);
    Future.delayed(Duration(milliseconds: animDuration + 1000), () {
      motionExitConfigurations.controller.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          onNoteAddedSubscription =
              notesReference.onChildAdded.listen((snapShot) {
            navigator(snapShot.snapshot.value["launcher"]);
          });
        }
      });
      motionExitConfigurations.controller.forward();
    });
  }

  navigator(String value) {
    switch (value) {
      case "home_screen":
        {
          navigateScreen(HomeScreen());
        }
        break;

      case "drawer_screen":
        {
          navigateScreen(DrawerScreen());
        }
        break;
      case "login_screen":
        {
//          navigateScreen(LoginScreen());
        }
        break;
      case "bottom_navigation_screen":
        {
          navigateScreen(BottomNavigationScreen());
        }
        break;
      case "social_login_screen":
        {
          navigateScreen(SocialLoginScreen());
        }
      break;
      case "web_view_screen":  {
        navigateScreen(WebViewScreen());
      }
      break;

      default:
        {
          navigateScreen(HomeScreen());
        }
        break;
    }
  }

  navigateScreen(Widget destinationScreen) {
    Navigator.pushReplacement(context,
        PageRouteBuilder(pageBuilder: (_, __, ___) => destinationScreen));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Motion<Column>(
      exitConfigurations: motionExitConfigurations,
      shouldRepeat: true,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        FlutterLogo(size: 120),
        SizedBox(height: 10),
        MotionElement(
          mode: MotionMode.FADE,
          child: Text(
            "Motion Widget",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
        )
      ],
    )));
  }

  @override
  void didChangePlatformBrightness() {
    final Brightness brightness = WidgetsBinding.instance.window.platformBrightness;
    print("brightness $brightness");
  }

  @override
  void dispose() {
    super.dispose();
    onNoteAddedSubscription.cancel();
    WidgetsBinding.instance.removeObserver(this);
  }
}
