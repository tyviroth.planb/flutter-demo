import 'package:flutter/material.dart';
import 'package:flutterdemo/configure/localization/language_wrapper.dart';
import 'package:flutterdemo/configure/localization/languge_config.dart';
import 'package:flutterdemo/configure/localization/local_share_preference.dart';

class Screen1 extends StatefulWidget {
  @override
  _Screen1State createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  @override
  Widget build(BuildContext context) {
    LanguageConfig languageConfig = LanguageWrapper.of(context);
    return Container(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(languageConfig.text("Message")),
            OutlineButton(
              onPressed: () {
                Navigator.of(context).pushNamed("/screen11");
              },
              child: Text("Click me!!"),
            ),

            OutlineButton(
              onPressed: () async {
                LocalSharePreference.saveLanguageToSharePreference("en");
                LanguageWrapper.updateLocale(context, "en");
              },
              child: Text("English"),
            ),

            OutlineButton(
              onPressed: () async{
                LocalSharePreference.saveLanguageToSharePreference("kh");
                LanguageWrapper.updateLocale(context, "kh");
              },
              child: Text("Khmer"),
            ),

          ],
        ),
      ),
    );
  }
}
