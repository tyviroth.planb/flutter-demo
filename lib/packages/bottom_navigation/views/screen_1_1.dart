import 'package:flutter/material.dart';

class Screen11 extends StatefulWidget {
  @override
  _Screen11State createState() => _Screen11State();
}

class _Screen11State extends State<Screen11> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text("screen 1_1"),
      ),
    );
  }
}
