import 'package:flutter/material.dart';
import 'package:flutterdemo/configure/navigation/nested_navigator.dart';
import 'package:flutterdemo/packages/bottom_navigation/views/screen_1.dart';
import 'package:flutterdemo/packages/bottom_navigation/views/screen_1_1.dart';
import 'package:flutterdemo/packages/bottom_navigation/views/screen_2.dart';
import 'package:flutterdemo/packages/bottom_navigation/views/screen_3.dart';

class BottomNavigationScreen extends StatefulWidget {
  @override
  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
}

class _BottomNavigationScreenState extends State<BottomNavigationScreen>  with WidgetsBindingObserver{

  final GlobalKey<NavigatorState> key1 = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> key2 = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> key3 = GlobalKey<NavigatorState>();

  List<Widget> widgetOptions;
  List<String> titleOptions = ["Home", "Card Gift", "Setting"];
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    widgetOptions = <Widget>[
      NestedNavigator(
        initialRoute: "/",
        routes: {
          "/": (context) => Screen1(),
          "/screen11": (context) => Screen11()
        },
        navigationKey: key1,
      ),
      NestedNavigator(
        initialRoute: "/",
        routes: {
          "/": (context) => Screen2(),
        },
        navigationKey: key2,
      ),
      NestedNavigator(
        initialRoute: "/",
        routes: {
          "/": (context) => Screen3(),
        },
        navigationKey: key3
      ),
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      if(selectedIndex == index && index == 0) {
        key1.currentState.popUntil(ModalRoute.withName("/"));
      } else if(selectedIndex == index && index == 1) {
        key2.currentState.popUntil(ModalRoute.withName("/"));
      } else {
        key3.currentState.popUntil(ModalRoute.withName("/"));
      }
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;
    return Scaffold(
      appBar: AppBar(
        title: Text("${titleOptions[selectedIndex]}"),
      ),
      body: IndexedStack(
        children: widgetOptions,
        index: selectedIndex,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Container(height: 0),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.card_giftcard),
              title: Container(height: 0),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Container(height: 0),
          ),
        ],
        unselectedFontSize: 14.0,
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: isDark ? Colors.red : Theme.of(context).primaryColor,
      ),
    );
  }

  @override
  void didChangePlatformBrightness() {
    final Brightness brightness = WidgetsBinding.instance.window.platformBrightness;
    print("brightness $brightness");
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }
}
