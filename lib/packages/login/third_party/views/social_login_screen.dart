import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterdemo/packages/login/third_party/services/facebook_service.dart';
import 'package:flutterdemo/packages/login/third_party/services/google_service.dart';
import 'package:flutterdemo/packages/login/third_party/services/sms_service.dart';

class SocialLoginScreen extends StatefulWidget {
  @override
  _SocialLoginScreenState createState() => _SocialLoginScreenState();
}

class _SocialLoginScreenState extends State<SocialLoginScreen> {

  TextEditingController phoneController = TextEditingController();
  TextEditingController codeController = TextEditingController();

  String verificationId;

  FirebaseAuth firebaseAuth;
  FirebaseUser firebaseUser;
  bool isLoggedIn = false;


  @override
  void initState() {
    super.initState();
    firebaseAuth = FirebaseAuth.instance;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      FirebaseUser user = await firebaseAuth.currentUser();
      if(user != null) {
        print("user ${user?.displayName}");
        setState(() {
          isLoggedIn = true;
          firebaseUser = user;
        });
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: isLoggedIn ? profileBox(firebaseUser) : Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              margin: EdgeInsets.only(bottom: 20),
              child:  TextFormField(
                decoration: InputDecoration(
                    hintText: "Phone number",
                    prefixIcon: Icon(Icons.phone),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () async{
                          dynamic result  = await SMSService.initiateSMS(phoneController.text);
                          print("dynamic result $result");

                          switch(result) {
                            case SMSEnum.codeSent: {

                            }
                            break;
                            case SMSEnum.success: {

                            }
                            break;
                            case SMSEnum.fail: {

                            }
                            break;
                            default : {
                              setState(() {
                                verificationId = result;
                              });
                            }
                            break;
                          }
                      },
                    )
                ),
                controller: phoneController,
                cursorColor: Theme.of(context).primaryColor,

              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              margin: EdgeInsets.only(bottom: 20),
              child:  TextFormField(
                controller: codeController,
                cursorColor: Theme.of(context).primaryColor,
              ),
            ),
            RaisedButton(
              color: Colors.blueAccent,
              textColor: Colors.white,
              onPressed: () async{
                dynamic result  = await SMSService.verifyCode(verificationId, codeController.text);

              },
              child: Text("        Get started         "),
            ),
            RaisedButton(
              color: Colors.blueAccent,
              textColor: Colors.white,
              onPressed: () async{
                FirebaseUser fUser  = await FacebookService.initiateFacebookLogin();
                setState(() {
                  isLoggedIn = true;
                  firebaseUser  = fUser;
                });
              },
              child: Text("Login with Facebook"),
            ),

            RaisedButton(
              color: Colors.deepOrange,
              textColor: Colors.white,
              onPressed: () async{
                FirebaseUser fUser = await GoogleService.initiateGoogleLogin();
                setState(() {
                  isLoggedIn = true;
                  firebaseUser  = fUser;
                });
              },
              child: Text("  Login with Google  "),
            )
          ],
        ),
      ),
    );
  }

  Widget profileBox(FirebaseUser firebaseUser) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Image.network("${firebaseUser?.photoUrl}", width: 64, height: 64,),
        ),
        Text("Name  : ${firebaseUser?.displayName}"),
        Text("Email : ${firebaseUser?.email}"),
        Text("Uid : ${firebaseUser?.uid}"),
        OutlineButton(
          onPressed: () {
            firebaseAuth.signOut();
            setState(() {
              isLoggedIn = false;
              firebaseUser = null;
            });
          },
          child: Text("Log out"),
        )

      ],
    );
  }
}
