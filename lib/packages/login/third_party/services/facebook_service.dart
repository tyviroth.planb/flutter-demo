import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class FacebookService {

  static Future<dynamic> initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult = await facebookLogin.logIn(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        return FacebookLoginStatus.error;
        break;
      case FacebookLoginStatus.cancelledByUser:
        return FacebookLoginStatus.cancelledByUser;
        break;
      case FacebookLoginStatus.loggedIn:
          var accessToken = facebookLoginResult.accessToken.token;
          var facebookAuth = FacebookAuthProvider.getCredential(accessToken: accessToken);
          var authResult = await FirebaseAuth.instance.signInWithCredential(facebookAuth);
          var token  = await authResult.user.getIdToken(refresh: true);
          return authResult.user;
        break;
    }
  }

  static void signOut() {
    FacebookService.signOut();
  }

}