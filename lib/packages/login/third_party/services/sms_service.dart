import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
enum SMSEnum {
  success,
  fail,
  codeSent,
  invalidCode,
  validCode
}

class SMSService {

  static Future<dynamic> initiateSMS(String phoneNumber ) async {

    final PhoneCodeSent smsCodeSent = (String verID, [int forceCodeResend]) {
      print('smsCodeSent');
      return SMSEnum.codeSent;
    };
    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verID) {
      print('auto retrieve');
      return verID;
    };

    final PhoneVerificationCompleted verificationSuccess = (AuthCredential credential) async{
      print('verificationSuccess');
      var authResult =
      await FirebaseAuth.instance.signInWithCredential(credential);
      var tokenResult = await authResult.user.getIdToken(refresh: true);
      return SMSEnum.success;
    };



    final PhoneVerificationFailed verificationFailed = (AuthException exception) {
      print('verificationFailed ${exception.message}');
      return SMSEnum.fail;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: "+855" + phoneNumber,
        codeAutoRetrievalTimeout: autoRetrieve,
        codeSent: smsCodeSent,
        timeout: const Duration(seconds: 15),
        verificationCompleted: verificationSuccess,
        verificationFailed: verificationFailed);
  }

  static Future<dynamic> verifyCode(String verificationId, String code) async{
    var credential = PhoneAuthProvider.getCredential(verificationId: verificationId, smsCode: code);
    try {
      var authResult =
      await FirebaseAuth.instance.signInWithCredential(credential);
      var tokenResult = await authResult.user.getIdToken(refresh: true);
      print("user phone number $tokenResult");
      return SMSEnum.validCode;
    } catch (e) {
      return SMSEnum.invalidCode;
    }
  }

}