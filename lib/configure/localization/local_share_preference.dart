import 'package:shared_preferences/shared_preferences.dart';

class LocalSharePreference {

  static readLanguageFromSharePreference() async {
    final prefs = await SharedPreferences.getInstance();
    final key = "language";
    final value = prefs.getString(key) ?? "en";
    return value.toString();
  }

  static saveLanguageToSharePreference(String language) async {
    final prefs = await SharedPreferences.getInstance();
    final key = "language";
    final value = language;
    prefs.setString(key, value);
  }

}




