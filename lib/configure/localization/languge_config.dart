import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LanguageConfig extends InheritedWidget {
  static Map<dynamic, dynamic> _localisedValues;

  LanguageConfig(
      {Key key, @required this.locale, @required Widget child})
      : assert(locale != null),
        assert(child != null),
        super(key: key, child: child) {
    if (_localisedValues == null) {
      load(locale);
    }
  }

  final String locale;

  @override
  bool updateShouldNotify(LanguageConfig oldWidget) {
    return this.locale != oldWidget.locale;
  }

  load(String locale) async {
    String jsonContent =
    await rootBundle.loadString("assets/localization/$locale.json");
    _localisedValues = json.decode(jsonContent);
  }

  String text(String key) {
    return _localisedValues[key] ?? "$key not found";
  }

  String getTextByKey(Map<String, dynamic> map, String key) {
    var languageKey;
    switch(locale) {
      case "zh": {
        languageKey = "${key}_ch";
      }
      break;

      case "kh": {
        languageKey = "${key}_kh";
      }
      break;

      default: {
        languageKey = "${key}_en";
      }
      break;
    }
    return map[languageKey] ?? "N/A";
  }

}
