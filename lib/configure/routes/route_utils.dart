import 'package:flutter/material.dart';
import 'package:flutterdemo/packages/login/third_party/views/social_login_screen.dart';
import 'package:flutterdemo/packages/splash_screen/views/home_screen.dart';
import 'package:flutterdemo/packages/splash_screen/views/splash_screen.dart';

class RouteUtil {

    static const initScreen = "/";
    static const homeScreen = "/home_screen";
    static const loginScreen = "/login_screen";

    static getRoute() {
        return <String, WidgetBuilder> {
            initScreen: (BuildContext context) => SplashScreen(),
            homeScreen: (BuildContext context) => HomeScreen(),
            loginScreen: (BuildContext context) => SocialLoginScreen()
        };
    }

}